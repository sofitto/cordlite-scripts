const Proxy = require('braid-client').Proxy;
const emea = new Proxy({url: 'https://emea-test.cordite.foundation/api/'}, onOpen, onClose, onError, {strictSSL: false})

let saltedTokenName = 'TOK-'+new Date().getTime()
let saltedAccountName = 'Acc-'+new Date().getTime()
let notary = "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB"

function onOpen() {
    console.log("connected to the emea test cordite node")

    emea.ledger.createTokenType(saltedTokenName, 2, notary).then( a => {
        console.log("Token with name " + saltedTokenName + " created")
        return emea.ledger.createAccount(saltedAccountName, notary)
    }).then( b => {
        console.log("Account with name " + saltedAccountName + " created")
        return emea.ledger.issueToken(saltedAccountName, 100, saltedTokenName, "First issuance", notary)
    }).then( c => {
        console.log("Tokens of type " + saltedTokenName + " issued to " + saltedAccountName)
        return emea.ledger.balanceForAccount(saltedAccountName)
    }).then( d => {
        bal = (d[0].quantity * d[0].displayTokenSize) + " " + d[0].token.symbol
        console.log("Balance for " + saltedAccountName + ": " + bal)
    }).catch(error => {
        console.error(error)
    })
}

function onClose() {
    console.log("closed")
}

function onError(err) {
    console.error(err)
}