'use strict';

const moment = require('moment');

const Proxy = require('braid-client').Proxy;
const emea = new Proxy({url: 'https://node1-sofitto.northeurope.cloudapp.azure.com:8080/api/'}, onOpen, onClose, onError, {strictSSL: false})

const isLastTransaction = (tx) => {
  return moment(tx.transactionTime).unix() > moment().subtract(2, 'minutes').unix();
}

const printTx = (tx, index) => {
  console.log(JSON.stringify(tx, null, 2));
}

const printAccountTransaction = ( txList ) => {
  console.log('start');
  const latestTransactions =  txList.filter(isLastTransaction);
  latestTransactions.forEach(printTx);
  console.log(`total: ${txList.length}, latest: ${latestTransactions.length}`);
}

const accounts = [
  "02e4561ef52dfc8632edb3d46dc0d698ae6706c8c72d57e6437e6e8b4da90de636", //9233 0000 0000 9969
  //"02ae717dedb7e7d6a4ebbf46f86172cb7c51395dcb536cfc2cdbcd4339a59495de", // 9233 1000 1000 0010
  //"03e594dcbff89d9b5ef850254d4dfb903f9b5844b72ca4b63df4ce02652752898c", //
  //"028d0377393a6a2e6ba29b2b2d7ad0be714b5a74c0dcccbf19c5d2ac6472178307", //
  ];

async function onOpen() {
    console.log("connected to the emea test cordite node")

    try {
        const promises = accounts.map(acc => emea.ledger.transactionsForAccount(acc, {pageNumber:1, pageSize: 100}));
        const transactionsTable = await Promise.all(promises);
        //console.log(transactionsTable.length);
        transactionsTable.forEach(printAccountTransaction);
    } catch (error) {
        console.error(error)
    } finally {
        process.exit()
    }

}

function onClose() {
    console.log("closed")
}

function onError(err) {
    console.error(err)
}
