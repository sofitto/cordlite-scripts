'use strict';

const Proxy = require('braid-client').Proxy;
const emea = new Proxy({url: 'https://node1-sofitto.northeurope.cloudapp.azure.com:8080/api/'}, onOpen, onClose, onError, {strictSSL: false})

const notary = "O=Notary,L=London,C=GB"
const tokenName = 'LBC1'

async function onOpen() {
    console.log("connected to the emea test cordite node")

    try {
        await emea.ledger.createTokenType(tokenName, 2, notary);
        console.log("Token with name " + tokenName + " created")
    } catch (error) {
        console.error(error)        
    } finally {
        process.exit()
    }    
}

function onClose() {
    console.log("closed")
}

function onError(err) {
    console.error(err)
}