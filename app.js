'use strict';

const Proxy = require('braid-client').Proxy;
const emea = new Proxy({url: 'https://emea-test.cordite.foundation/api/'}, onOpen, onClose, onError, {strictSSL: false})
//const emea = new Proxy({url: 'http://localhost:5006/api/'}, onOpen, onClose, onError, {strictSSL: false})

let saltedTokenName = 'LBC-'+new Date().getTime()
let saltedAccountName = 'Acc-'+new Date().getTime()
let saltedAccountName2 = 'Acc-'+new Date().getTime()
let notary = "OU=Cordite Foundation, O=Cordite Guardian Notary, L=London, C=GB"

async function onOpen() {
    console.log("connected to the emea test cordite node")

    try {

        await emea.ledger.createTokenType(saltedTokenName, 2, notary);
        console.log("Token with name " + saltedTokenName + " created")

        await emea.ledger.createAccount(saltedAccountName, notary)
        console.log("Account with name " + saltedAccountName + " created")

        await emea.ledger.createAccount(saltedAccountName2, notary)        
        console.log("Account with name " + saltedAccountName2 + " created")

        await emea.ledger.issueToken(saltedAccountName, 100, saltedTokenName, "First issuance", notary)

        console.log("Tokens of type " + saltedTokenName + " issued to " + saltedAccountName)
        let d = await emea.ledger.balanceForAccount(saltedAccountName)        
        let bal = (d[0].quantity * d[0].displayTokenSize) + " " + d[0].token.symbol
        console.log("Balance for " + saltedAccountName + ": " + bal)

        await emea.ledger.transferAccountsToAccount(2, saltedTokenName, saltedAccountName, saltedAccountName2, "!!!" , notary);
        
        d = await emea.ledger.balanceForAccount(saltedAccountName2)        
        bal = (d[0].quantity * d[0].displayTokenSize) + " " + d[0].token.symbol
        console.log("Balance for " + saltedAccountName2+ ": " + bal)
    } catch (error) {
        console.error(error)        
    }    
}

function onClose() {
    console.log("closed")
}

function onError(err) {
    console.error(err)
}