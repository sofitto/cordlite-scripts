'use strict';

const Proxy = require('braid-client').Proxy;
const emea = new Proxy({url: 'https://node1-sofitto.northeurope.cloudapp.azure.com:8080/api/'}, onOpen, onClose, onError, {strictSSL: false})

const notary = "O=Notary,L=London,C=GB"
const accounts = ["02ae717dedb7e7d6a4ebbf46f86172cb7c51395dcb536cfc2cdbcd4339a59495de", "03e594dcbff89d9b5ef850254d4dfb903f9b5844b72ca4b63df4ce02652752898c", "028d0377393a6a2e6ba29b2b2d7ad0be714b5a74c0dcccbf19c5d2ac6472178307"];

async function onOpen() {
    console.log("connected to the emea test cordite node")

    try {
        const promises = accounts.map(acc => emea.ledger.createAccount(acc, notary));
        await Promise.all(promises)
        console.log("Accounts created")
    } catch (error) {
        console.error(error)        
    } finally {
        process.exit()
    }    
}

function onClose() {
    console.log("closed")
}

function onError(err) {
    console.error(err)
}